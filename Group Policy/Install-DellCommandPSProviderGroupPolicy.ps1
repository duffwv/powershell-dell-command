﻿$invocation_path = Split-Path -Path $MyInvocation.MyCommand.Path

$jp = @{
    'Path' = $invocation_path
    'ChildPath' = "dell_module.xml"
}
$dell_module_path = Join-Path @jp

$dell_module = Import-Clixml -Path $dell_module_path

If(-not (Test-Path -Path $dell_module['local_directory'])) {
    New-Item -ItemType Container -Force -Path $dell_module['local_directory']

    Get-ChildItem $dell_module['deployment_root'] | 
    Copy-Item -Recurse -Destination $dell_module['local_directory']
}

$driver = driverquery.exe /v /fo csv | 
ConvertFrom-CSV | 
Where {$_.'Module Name' -match 'Dcdbas'}

If(-not $driver) {
    $jp = @{
        'Path' = $dell_module['local_directory']
        'ChildPath' = $dell_module['hapi_root']
    }
    $hapi_root = Join-Path @jp
    $jp = @{
        'Path' = $hapi_root
        'ChildPath' = $dell_module['hapi_installer']
    }
    $hapi_installer = Join-Path @jp

    Set-Location -Path $Using:hapi_root
    Invoke-Expression -Command "&'$Using:hapi_installer'"
}
