﻿Function Load-DellFunctionSettings {
    $jp = @{
        'Path' = $PSScriptRoot
        'ChildPath' = "dell_bios_settings.xml"
    }
    $settings_path = Join-Path @jp

    $jp = @{
        'Path' = $PSScriptRoot
        'ChildPath' = "dell_module.xml"
    }
    $dell_module_path = Join-Path @jp

    $Script:dell_bios_settings = Import-Clixml -Path $settings_path
    $Script:dell_module = Import-Clixml -Path $dell_module_path

    $jp = @{
        'Path' = $dell_module['local_directory']
        'ChildPath' = $dell_module['dell_psprovider_installer']
    }
    $Script:module_installer = Join-Path @jp
}

Function Get-DellBIOSSettings {
[CmdletBinding()]
Param(
    [Parameter(
        Position=0,
        ValueFromPipeline=$True
    )]
    [String]
    $Name = 'localhost'
)

Begin {
    Load-DellFunctionSettings
}

Process {
    $session = New-PSSession -ComputerName $Name

    If($session) {
        Invoke-Command -Session $session -ScriptBlock { 
            If(-not (Test-Path $Using:module_installer)) {
                Throw "Module not installed on $Using:Name"
            }
            &$Using:module_installer
            $properties = @{
                'ComputerName' = $Using:Name
            }
            ForEach($setting in $Using:dell_bios_settings) {
                $jp = @{
                    'Path' = $setting.Root
                    'ChildPath' = $setting.Name
                }
                $properties[$setting.Name] = (
                    Get-Item -Path (Join-Path @jp)
                ).CurrentValue
            }
            [pscustomobject]$properties
        }
    } Else {
        Throw "Unable to establish connection to $Name"
    }

    If($session) {
        Remove-PSSession -Session $session
    }
}
}

Function Verify-DellBIOSSettings {
[CmdletBinding()]
Param(
    [Parameter(
        Position=0,
        ValueFromPipeline=$True
    )]
    [String]
    $Name = 'localhost'
)

Begin {
    Load-DellFunctionSettings
}

Process {
    $session = New-PSSession -ComputerName $Name

    If($session) {
        Invoke-Command -Session $session -ScriptBlock { 
            If(-not (Test-Path $Using:module_installer)) {
                Throw "Module not installed on $Using:Name"
            }
            &$Using:module_installer
            $properties = @{
                'ComputerName' = $Using:Name
            }
            $valid = $True
            ForEach($setting in $Using:dell_bios_settings) {
                $jp = @{
                    'Path' = $setting.Root
                    'ChildPath' = $setting.Name
                }
                $value = (
                        Get-Item -Path (Join-Path @jp)
                    ).CurrentValue

                If($value) {
                    If(
                        $setting.Value -ne $value
                    ) {
                        $valid = $False
                        Break
                    }
                }
                Else {
                    Throw "Unable to obtain BIOS value to verify"
                }
            }
            Return $valid
        }
    } Else {
        Throw "Unable to establish connection to $Name"
    }

    If($session) {
        Remove-PSSession -Session $session
    }
}
}

Function Set-DellBIOSSettings {
[CmdletBinding()]
Param(
    [Parameter(
        Position=0,
        ValueFromPipeline=$True
    )]
    [String]
    $Name = 'localhost'
)

Begin {
    Load-DellFunctionSettings
}

Process {
    $session = New-PSSession -ComputerName $Name

    If($session) {
        Invoke-Command -Session $session -ScriptBlock { 
            If(-not (Test-Path $Using:module_installer)) {
                Throw "Module not installed on $Using:Name"
            }
            &$Using:module_installer
            ForEach($setting in $Using:dell_bios_settings) {
                $jp = @{
                    'Path' = $setting.Root
                    'ChildPath' = $setting.Name
                }
                $current_value = (
                    Get-Item -Path (Join-Path @jp)
                ).CurrentValue
                If($current_value -ne $setting.Value) {
                    Set-Item -Path (Join-Path @jp) -Value $setting.Value
                }
            }
        }
    } Else {
        Throw "Unable to establish connection to $Name"
    }

    If($session) {
        Remove-PSSession -Session $session
    }
}
}

Function Install-DellCommandPSProvider {
[CmdletBinding()]
Param(
    [Parameter(
        Mandatory=$True,
        Position=0,
        ValueFromPipeline=$True
    )]
    [String]
    $ComputerName
)
Begin {
    Load-DellFunctionSettings
    $drive = (Split-Path -Path $dell_module['local_directory'] -Qualifier)[0]
    $local_path = Split-Path -Path $dell_module['local_directory'] -NoQualifier
}
Process {
    $admin_root = Join-Path -Path "\\$ComputerName\$drive`$\" -ChildPath $local_path

    If(-not (Test-Path -Path $admin_root)) {
        New-Item -ItemType Container -Force -Path $admin_root

        Get-ChildItem $dell_module['deployment_root'] | 
        Copy-Item -Recurse -Destination $admin_root
    }

    $session = New-PSSession -ComputerName $ComputerName

    If($session) {
        $jp = @{
            'Path' = $dell_module['local_directory']
            'ChildPath' = $dell_module['hapi_root']
        }
        $hapi_root = Join-Path @jp
        $jp = @{
            'Path' = $hapi_root
            'ChildPath' = $dell_module['hapi_installer']
        }
        $hapi_installer = Join-Path @jp
        Invoke-Command -Session $session -ScriptBlock { 
                $driver = driverquery.exe /v /fo csv | 
                ConvertFrom-CSV | 
                Where {$_.'Module Name' -match 'Dcdbas'}

                If(-not $driver) { 
                    Set-Location -Path $Using:hapi_root
                    Invoke-Expression -Command "&'$Using:hapi_installer'"
                }
        }    
    } Else {
        Throw "Unable to establish connection to $Name"
    }   
    If($session) {
        Remove-PSSession -Session $session
    } 
}
}